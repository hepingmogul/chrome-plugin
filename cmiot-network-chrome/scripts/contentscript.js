﻿(function () {
  function selector(name) {
    const ls = document.querySelectorAll(name)
    return ls ? ls.length === 1 ? ls[0] : ls : null
  }
  function hide(el) {
    if (el) el.style.display = 'none'
  }
  function show(el) {
    if (el) el.style.display = 'block'
  }
  if (new RegExp(window.location.hostname, 'g').test("192.168.218.97")) {
    hide(selector('#agentClientLoading'))
    show(selector('#loginForm'))
  }
})()